#version 330 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 frame;

uniform mat4 pr_matrix;
uniform mat4 vw_matrix;
uniform mat4 ml_matrix;

out DATA
{
    vec2 tc;
    vec3 position;
}vs_out;

uniform vec2 tex_offset = vec2(0, 0);
uniform float numberOfRows;
uniform vec2 offset;

void main()
{
    gl_Position = pr_matrix * vw_matrix * ml_matrix * position;
    //vs_out.tc = frame.xy + (tex_offset * frame.zw);
    vs_out.tc = (frame.xy / numberOfRows) + offset;
    vs_out.position = vec3(vw_matrix * ml_matrix * position);
}