package org.lwengine.scene;

import org.lwengine.gameobject.GameObject;

import java.util.ArrayList;
import java.util.List;

public abstract class Scene {

    protected List<GameObject> objects = new ArrayList<GameObject>();

    public void addObject(GameObject object){
        objects.add(object);
    }

    public abstract void update();

    public abstract void render();
}
