package org.lwengine.scene;

public class SceneManager {

    private Scene activeScene;
    private Scene defaultScene;

    public SceneManager(){
        defaultScene = new DefaultScene();
        setActiveScene(defaultScene);
    }

    public SceneManager(Scene scene){
        setActiveScene(scene);
    }

    public void setActiveScene(Scene scene){
        activeScene = scene;
    }

    public void update(){
        if(activeScene != null) activeScene.update();
    }

    public void render(){
        if(activeScene != null) activeScene.render();
    }
}
