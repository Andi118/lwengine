package org.lwengine.scene.tilemap;

import org.lwengine.debug.Debug;
import org.lwengine.graphics.Sprite;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TileMap {
    // TODO: implement method for creating tile maps from Tiled-Json
    private Sprite tileSheet;
    private float x, y;
    private int[] tiles;
    private int worldWidth, worldHeight;

    public TileMap(Sprite tileSheet, int tileSize, String levelFile){
        this.tileSheet = tileSheet;
        tiles = createTileArray(levelFile);
        Debug.log("world width: " + worldWidth, "world height: " + worldHeight, "Tiles: " + tiles.length, "Tile size: " + tileSize);
        generateTileMap();
    }

    private int[] createTileArray(String path){
        int[] result = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line;
            while((line = reader.readLine()) != null){
                if(line.contains("\"height\"") && worldHeight == 0){
                    line.trim();
                    String height = line.substring(line.indexOf(":") + 1, line.indexOf(","));
                    worldHeight = Integer.parseInt(height);
                }
                if(line.contains("\"width\"") && worldWidth == 0){
                    //line.trim();
                    String width = line.substring(line.indexOf(":") + 1, line.indexOf(","));
                    worldWidth = Integer.parseInt(width);
                }
                if(line.contains("\"data\"")){
                    String mapData = line.substring(line.indexOf("[") + 1, line.indexOf("]"));
                    String[] numbers = mapData.split(",");
                    result = new int[numbers.length];
                    for(int i = 0; i <numbers.length; i++){
                        result[i] = Integer.parseInt(numbers[i].trim());
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Debug.error("File not found! : " + this.getClass().getName());
        } catch(IOException e){
            e.printStackTrace();
            Debug.error("IOException raised");
        }

        return result;
    }

    Tile[] map;
    private void generateTileMap(){
        map = new Tile[tiles.length];
        for(int y = 0; y < worldHeight; y++){
            for(int x = 0; x < worldWidth; x++){
                map[x + y * worldWidth] = new Tile( x   , (worldHeight - y) , tiles[x + y * worldWidth] - 1, tileSheet);
            }
        }

        Debug.log("Map: " + map.length);
    }

    public Tile getTile(int index){
        if(index > tiles.length){
            Debug.error("Can't get tile on index " + index);
            return map[0];
        } else {
            return map[index];
        }
    }

    public int getMapSize(){
        return map.length;
    }


}
