package org.lwengine.scene.tilemap;

import org.joml.Vector3f;
import org.lwengine.debug.Debug;
import org.lwengine.gameobject.GameObject;
import org.lwengine.graphics.Sprite;

public class Tile extends GameObject{


    public Tile(float x, float y, int index, Sprite tileSheet){
        this.sprite = tileSheet;
        this.textureIndex = index;
        this.position = new Vector3f(x, y, 0.0f);
    }

    public Tile(int x, int y, float posX, float posY, Sprite tileSheet){
        this.sprite = tileSheet;
        this.spriteOffsetX = x;
        this.spriteOffsetY = y;
        this.position = new Vector3f(posX, posY, 0.0f);
    }


}
