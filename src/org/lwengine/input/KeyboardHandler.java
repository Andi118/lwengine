package org.lwengine.input;

import org.lwjgl.glfw.GLFWKeyCallback;

public class KeyboardHandler extends GLFWKeyCallback {

    public static boolean keys[] = new boolean[400];

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
        keys[key] = action != 0;
        Keyboard.keys[key] = action != 0;
        Keyboard.action[key] = action;
    }
}
