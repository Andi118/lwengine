package org.lwengine.input;

import org.lwengine.debug.Debug;

public class Keyboard {

    public static boolean keys[] = new boolean[400];
    public static int action[] = new int[400];

    public final static int KEY_RELEASE = 0;
    public final static int KEY_PRESS = 1;
    public final static int KEY_REPEAT = 2;

    public final static int KEY_W = 87;
    public final static int KEY_A = 65;
    public final static int KEY_S = 83;
    public final static int KEY_D = 68;
    public final static int KEY_F = 70;

    public static boolean isKeyDown(int keycode){
        return keys[keycode];
    }
    public static boolean isKeyPressed(int keycode){
        // TODO: key press
        if(action[keycode] == KEY_PRESS){
            action[keycode] = KEY_REPEAT;
            Debug.log("Pressed: " + keycode);
            return true;
        }
        return false;
    }
}
