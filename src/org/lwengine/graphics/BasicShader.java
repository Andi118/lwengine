package org.lwengine.graphics;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.lwengine.gameobject.Camera;
import org.lwengine.utils.Maths;

public class BasicShader extends Shader {

    private static final String VERTEX_FILE = "res/shader/basic.vert";
    private static final String FRAGMENT_FILE = "res/shader/basic.frag";

    private int location_transformationMatrix;
    private int location_perspectiveMatrix;
    private int location_viewMatrix;
    private int location_texOffset;
    private int location_numberOfRows;
    private int location_offset;

    public BasicShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void getAllUniformLocation() {
        location_transformationMatrix = super.getUniformLocation("ml_matrix");
        location_perspectiveMatrix = super.getUniformLocation("pr_matrix");
        location_viewMatrix = super.getUniformLocation("vw_matrix");
        location_texOffset = super.getUniformLocation("tex_offset");
        location_numberOfRows = super.getUniformLocation("numberOfRows");
        location_offset = super.getUniformLocation("offset");
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "frame");
    }

    public void loadNumberOfRows(int numberOfRows){
        super.loadFloat(location_numberOfRows, numberOfRows);
    }

    public void loadOffset(float x, float y){
        super.loadVector2(location_offset, new Vector2f(x, y));
    }

    public void loadTexOffset(float x, float y){
        super.loadUniform2f(location_texOffset, x, y);
    }

    public void loadViewMatrix(Camera camera){
        Matrix4f viewMatrix = Maths.createViewMatrix(camera);
        super.loadMatrix4f(location_viewMatrix, viewMatrix);
    }

    public void loadPerspectiveMatrix(Matrix4f matrix){
        super.loadMatrix4f(location_perspectiveMatrix, matrix);
    }

    public void loadTransformationMatrix(Matrix4f matrix){
        super.loadMatrix4f(location_transformationMatrix, matrix);
    }
}
