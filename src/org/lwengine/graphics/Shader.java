package org.lwengine.graphics;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.lwengine.debug.Debug;
import org.lwengine.gameobject.Camera;
import org.lwjgl.BufferUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;

public abstract class Shader {

    private int programID;
    private int vertexShaderID;
    private int fragmentShaderID;

    private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);

    public Shader(String vertexFile, String fragmentFile){
        vertexShaderID = loadShader(vertexFile, GL_VERTEX_SHADER);
        fragmentShaderID = loadShader(fragmentFile, GL_FRAGMENT_SHADER);

        programID = glCreateProgram();

        glAttachShader(programID, vertexShaderID);
        glAttachShader(programID, fragmentShaderID);
        bindAttributes();
        glLinkProgram(programID);
        glValidateProgram(programID);
        getAllUniformLocation();
    }

    protected abstract void getAllUniformLocation();

    protected abstract void bindAttributes();

    protected int getUniformLocation(String uniformName){
        return glGetUniformLocation(programID, uniformName);
    }

    protected void bindAttribute(int attribute, String variableName){
        glBindAttribLocation(programID, attribute, variableName);
    }

    protected void loadFloat(int location, float value){
        glUniform1f(location, value);
    }

    protected void loadUniform2f(int location, float v1, float v2){
        glUniform2f(location, v1, v2);
    }

    protected void loadMatrix4f(int location, Matrix4f matrix){
        matrix.get(matrixBuffer);
        glUniformMatrix4fv(location, false, matrixBuffer);
    }

    protected void loadVector2(int location, Vector2f vector){
        glUniform2f(location, vector.x, vector.y);
    }

    public void loadPerspectiveMatrix(Matrix4f matrix){

    }

    public void loadTransformationMatrix(Matrix4f matrix){

    }

    public void loadViewMatrix(Camera camera){

    }

    public void loadOffset(float x, float y){

    }

    public void loadNumberOfRows(int numberOfRows){

    }

    public void loadTexOffset(float x, float y){

    }

    public void start(){
        glUseProgram(programID);
    }

    public void stop(){
        glUseProgram(0);
    }

    public void cleanup(){
        stop();
        glDetachShader(programID, vertexShaderID);
        glDetachShader(programID, fragmentShaderID);
        glDeleteShader(vertexShaderID);
        glDeleteShader(fragmentShaderID);
        glDeleteProgram(programID);
    }

    private static int loadShader(String file, int type){
        StringBuilder shaderSource = new StringBuilder();
        try{
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null){
                shaderSource.append(line).append("//\n");
            }
            reader.close();
        } catch (IOException e){
            e.printStackTrace();
            Debug.log("could not load shader file " + file);
            System.exit(-1);
        }

        int shaderID = glCreateShader(type);
        if(shaderID == 0){
            Debug.log("ERROR!!!");
            System.exit(-1);
        }
        glShaderSource(shaderID, shaderSource);
        glCompileShader(shaderID);
        if(glGetShaderi(shaderID, GL_COMPILE_STATUS) == GL_FALSE){
            Debug.log(glGetShaderInfoLog(shaderID, 500));
            System.exit(-1);
        }
        return shaderID;
    }
}
