package org.lwengine.graphics;

import org.joml.Matrix4f;
import org.lwengine.gameobject.Camera;
import org.lwengine.gameobject.GameObject;
import org.lwengine.utils.Maths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Renderer2D {

    private Matrix4f perspectiveMatrix = Maths.createOrtho();

    private Shader shader;
    private ObjectRenderer renderer;

    private Map<Sprite, List<GameObject>> objects = new HashMap<Sprite, List<GameObject>>();

    public Renderer2D(){
        shader = new BasicShader();
        renderer = new ObjectRenderer(shader, perspectiveMatrix);
    }

    public Renderer2D(Shader shader){
        renderer = new ObjectRenderer(shader, perspectiveMatrix);
        this.shader = shader;
    }

    public void render(Camera camera){
        shader.start();
        shader.loadViewMatrix(camera);
        renderer.render(objects);
        shader.stop();

        objects.clear();
    }

    public void processObject(GameObject object){
        Sprite model = object.getSprite();
        List<GameObject> batch = objects.get(model);
        if(batch != null){
            batch.add(object);
        } else {
            List<GameObject> newBatch = new ArrayList<>();
            newBatch.add(object);
            objects.put(model, newBatch);
        }
    }
}
