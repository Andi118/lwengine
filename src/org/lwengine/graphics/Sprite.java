package org.lwengine.graphics;

public class Sprite {

    private static Loader loader = new Loader();
    private Model model;
    private Texture texture;

    private float size = 1.0f;
    private float spriteX = 1.0f;
    private float spriteY = 1.0f;

    /* Texture Atlas */
    private int numberOfRows = 1;

    float[] vertices = {-size, -size, 0.0f, -size, size, 0.0f, size, size, 0.0f, size, -size, 0.0f};
    int[] indices = {0, 1, 2, 2, 3, 0};
    float[] texCoords = {
            0, 1.0f / spriteY, 1.0f / spriteX, 1.0f / spriteY,
            0, 0, 1.0f / spriteX, 1.0f / spriteY,
            1.0f / spriteX, 0, 1.0f / spriteX, 1.0f / spriteY,
            1.0f / spriteX, 1.0f / spriteY, 1.0f/spriteX, 1.0f /spriteY
    };

    public Sprite(Texture texture, float size, float spriteX, float spriteY){
        setSpriteParams(size, spriteX, spriteY);
        model = loader.loadToVAO(vertices, indices, texCoords);
        this.texture = texture;
    }

    public Sprite(Texture texture, Model model){
        setSpriteParams(size, spriteX, spriteY);
        this.model = model;
        this.texture = texture;
    }

    public void setSpriteParams(float size, float spriteX, float spriteY){
        this.size = size;
        this.spriteX = spriteX;
        this.spriteY = spriteY;
        createVIT();
    }

    private void createVIT(){
        vertices = new float[]{-size, -size, 0.3f, -size, size, 0.3f, size, size, 0.3f, size, -size, 0.3f};
        indices = new int[]{0, 1, 2, 2, 3, 0};
        texCoords = new float[]{0, 1.0f / spriteY, 1.0f / spriteX, 1.0f / spriteY, 0, 0, 1.0f / spriteX, 1.0f / spriteY,
                1.0f / spriteX, 0, 1.0f / spriteX, 1.0f / spriteY, 1.0f / spriteX, 1.0f / spriteY, 1.0f/spriteX, 1.0f /spriteY
        };
    }

    public Texture getTexture(){
        return texture;
    }

    public Model getModel(){
        return model;
    }

    public void setSpriteSize(float size){
        this.size = size;
    }

    public void setSpriteX(float spriteX){
        this.spriteX = spriteX;
    }

    public void setSpriteY(float spriteY){
        this.spriteY = spriteY;
    }

    public int getNumberOfRows() { return numberOfRows; }

    public void setNumberOfRows(int value){ numberOfRows = value;}
}
