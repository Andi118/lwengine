package org.lwengine.graphics;

import org.joml.Matrix4f;
import org.lwengine.gameobject.GameObject;
import org.lwengine.scene.tilemap.Tile;
import org.lwengine.utils.Maths;

import java.util.List;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

public class ObjectRenderer {

    private Shader shader;

    public ObjectRenderer(Shader shader, Matrix4f perspectiveMatrix){
        this.shader = shader;
        shader.start();
        shader.loadPerspectiveMatrix(perspectiveMatrix);
        shader.stop();
    }

    public void render(Map<Sprite, List<GameObject>> objects){
        for(Sprite model : objects.keySet()){
            prepareTexModel(model);
            List<GameObject> batch = objects.get(model);
            for(GameObject object : batch){
                prepareInstance(object);
                // texoffset uniforms etc
                glDrawElements(GL_TRIANGLES, model.getModel().getVertexCount(), GL_UNSIGNED_INT, 0);
            }
        }
    }

    private void prepareTexModel(Sprite model){
        Model rawModel = model.getModel();
        glBindVertexArray(rawModel.getVaoID());
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        shader.loadNumberOfRows(model.getNumberOfRows());

        glActiveTexture(GL_TEXTURE0);
        model.getTexture().bind();
    }

    private void prepareInstance(GameObject object){
        Matrix4f transformationMatrix = Maths.createTransformationMatrix(object.getPosition(), object.getRotation());
        shader.loadTransformationMatrix(transformationMatrix);
        if(object instanceof Tile)shader.loadOffset(object.getTextureXOffset(), object.getTextureYOffset());
        else shader.loadOffset(object.getSpriteOffsetX(), object.getSpriteOffsetY());
    }
}
