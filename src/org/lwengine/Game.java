package org.lwengine;

import org.lwengine.debug.Debug;
import org.lwengine.scene.Scene;
import org.lwengine.scene.SceneManager;

public class Game {

    private GameWindow window;
    private static SceneManager sceneManager;

    private boolean running = false;
    private static boolean showFPS = false;

    public Game(String title, int width, int height){
        window = new GameWindow(title, width, height);
        sceneManager = new SceneManager();
        running = true;
        Debug.log("Game started!");
    }

    public static void fps(boolean show){
        showFPS = show;
    }

    public void setScene(Scene scene){
        sceneManager.setActiveScene(scene);
    }

    public void update(){
        window.update();
        sceneManager.update();
    }

    public void render(){
        window.clear();

        sceneManager.render();

        window.render();
    }

    public void run(){
        long lastTime = System.nanoTime();
        long time = System.currentTimeMillis();
        final double ns = 1_000_000_000.0 / 60.0;
        double delta = 0;
        int frames = 0, updates = 0;
        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime) /ns;
            lastTime = now;
            while(delta >= 1){
                update();
                updates++;
                delta--;
            }
            render();
            frames++;

            if((System.currentTimeMillis() - time) > 1000 && Game.showFPS ){
                time += 1000;
                Debug.log("UPS: " + updates + " FPS: " + frames);
                updates= 0; frames = 0;
            }
            if(window.windowShouldClose())
                running = false;
        }
        window.terminate();
        Debug.closeWriter();
    }

    public static SceneManager getSceneManager(){
        return sceneManager;
    }
}
