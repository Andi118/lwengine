package org.lwengine.animation;

import org.lwengine.gameobject.GameObject;

public class AnimationClip {

    private Animator animator;
    private String name;
    private int length;
    private int startX, startY;
    long time;
    int tickTime;
    private boolean loop;
    public boolean isFinished = false;
    int index = 0;

    public AnimationClip(){

    }

    public AnimationClip(Animator animator, String name, int length, int startX, int startY, int ms, boolean loop){
        this.name = name;
        this.length = length;
        this.animator = animator;
        this.tickTime = ms;
        this.startX = startX;
        this.startY = startY;
        this.loop = loop;
        time = System.currentTimeMillis() + tickTime;
        index = startX;
        animator.addAnimation(name, this);
    }

    public void play(GameObject target){
        if(index == length - 1  && !loop) {
            index = 0;
            isFinished = true;
        }
        if(System.currentTimeMillis() > time ){
            target.setSpriteOffset(startY * 21 + startX + index , startY);
            index++;
            time = System.currentTimeMillis() + tickTime;
            if((index - startX) > length - 1) index = startX;
        }

    }

    public String getName(){
        return name;
    }
}
