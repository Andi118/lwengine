package org.lwengine.animation;

import org.lwengine.debug.Debug;
import org.lwengine.gameobject.GameObject;

import java.util.HashMap;
import java.util.Map;

public class Animator {

    private GameObject target;
    private AnimationClip activeAnimation, oldAnim;
    private Map<String, AnimationClip> animations = new HashMap<>();

    public Animator(GameObject target){
        this.target = target;
        oldAnim = new AnimationClip();
    }

    public void addAnimation(String name, AnimationClip animation){
        if(animations.get(name) != null){
            Debug.error("Animation '" + name + "' already exists!");
            return;
        }
        animations.put(name, animation);
    }

    public void playAnimation(String animation){
        animations.get(animation).play(target);
    }

    public void setActiveAnimation(String name){
        if(animations.get(name) == null){
            Debug.error("No animation with name '" + name + "' found!");
            return;
        }
        if(activeAnimation != null) oldAnim = activeAnimation;
        activeAnimation = animations.get(name);
    }

    public void animate(){
        if(activeAnimation != null)
        {
            activeAnimation.play(target);
            if(!oldAnim.equals(activeAnimation)) {
                oldAnim.isFinished = true;
                oldAnim.index = 0;
            }
        }
    }
}
