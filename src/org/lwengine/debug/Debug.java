package org.lwengine.debug;

import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Debug {

    public static boolean logToFile = false;
    public static File logFile = new File("engine-log.txt");

    public static FileOutputStream fos;
    private static BufferedWriter bw;

    static {
        try {
            fos = new FileOutputStream(logFile);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void log(String message){
        System.out.println("[Engine]: " + message);
        if(logToFile) {
            writeToFile(message);
        }
    }

    public static void log(String... msg){
        System.out.println("[Engine]: \t" + msg[0]);
        for(int i = 1; i < msg.length; i++){
            System.out.println("\t\t\t" + msg[i]);
        }
    }

    public static void closeWriter(){
        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void error(String message){
        System.err.println("[ERROR]: " + message);
    }

    private static void writeToFile(String message){
        try{
            fos = new FileOutputStream(logFile);
            String timeStamp = new SimpleDateFormat("HH:mm:ss").format(new Timestamp(System.currentTimeMillis()));
            bw.write("[" + timeStamp + "]: " + message);
            bw.newLine();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
