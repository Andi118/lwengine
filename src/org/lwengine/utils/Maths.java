package org.lwengine.utils;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwengine.gameobject.Camera;

public class Maths {

    public static Matrix4f createTransformationMatrix(Vector3f position, Vector3f rotation){
        Matrix4f matrix = new Matrix4f();
        matrix.identity();
        matrix.translate(position, matrix);
        // TODO: rotation and scale
        return matrix;
    }

    private static final float SIZE = 11f;
    private static final float RATIO = 9.0f/16.0f;

    public static Matrix4f createOrtho(){
        Matrix4f matrix = new Matrix4f();
        matrix.ortho(-SIZE, SIZE, -SIZE * RATIO, SIZE * RATIO, -1.0f, 2.0f);
        return matrix;
    }

    public static Matrix4f createViewMatrix(Camera camera){
        Matrix4f matrix = new Matrix4f();
        matrix.identity();

        Vector3f pos;
        if(camera != null) pos = camera.getPosition();
        else  pos = new Vector3f(0, 0, 0);
        Vector3f npos = new Vector3f(-pos.x, -pos.y, -pos.z);
        matrix.translate(npos, matrix);

        return matrix;
    }
}
