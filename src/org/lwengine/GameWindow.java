package org.lwengine;

import org.lwengine.debug.Debug;
import org.lwengine.input.KeyboardHandler;
import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

public class GameWindow {

    private String windowTitle;
    private static int windowWidth, windowHeight;

    private long window;
    private boolean isFullscreen = false;

    public GameWindow(String title, int width, int height){
        windowTitle = title;
        windowWidth = width;
        windowHeight = height;

        Debug.log("LWJGL " + Version.getVersion());
        initWindow();
        Debug.log("OpenGL " + glGetString(GL_VERSION));
    }

    private void initWindow(){
        GLFWErrorCallback.createPrint(System.err).set();

        if(!glfwInit())
            throw new IllegalStateException("Failed to initialize GLFW");

        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        window = glfwCreateWindow(windowWidth, windowHeight, windowTitle, NULL, NULL);

        if(window == NULL)
            throw new RuntimeException("Failed to create GLFW window");

        try(MemoryStack stack = stackPush()){
            IntBuffer pWidth = stack.mallocInt(1);
            IntBuffer pHeight = stack.mallocInt(1);

            glfwGetWindowSize(window, pWidth, pHeight);
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            glfwSetWindowPos(window, (vidmode.width() - pWidth.get(0)) / 2, (vidmode.height() - pHeight.get(0)) / 2);
            glfwShowWindow(window);
        }

        glfwSetKeyCallback(window, new KeyboardHandler());
        glfwMakeContextCurrent(window);
        glfwSwapInterval(1);

        GL.createCapabilities();
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        glfwSetWindowSizeCallback(window, new GLFWWindowSizeCallback(){
            @Override
            public void invoke(long window, int width, int height){
                windowWidth = width;
                windowHeight = height;
                glViewport(0, 0, width, height);
            }
        });

        glEnable(GL_DEPTH_TEST);
    }

    public void update(){
        int error = glGetError();
        if(error != GL_NO_ERROR)
            Debug.error(Integer.toString(error));

        glfwPollEvents();
    }

    public void render(){
        glfwSwapBuffers(window);
    }

    public void clear(){
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(0f, 0f, 0.0f, 1.0f);
    }

    public boolean windowShouldClose(){
        return glfwWindowShouldClose(window);
    }

    public void terminate(){
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);
        glfwTerminate();
        glfwSetErrorCallback(null).free();
        Debug.log("window terminated");
    }
}
