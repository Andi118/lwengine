package org.lwengine.gameobject;

import org.joml.Vector3f;
import org.lwengine.graphics.Sprite;

public abstract class GameObject {

    protected Vector3f position = new Vector3f(0, 0, 0);
    protected Vector3f rotation = new Vector3f(0, 0, 0);
    protected Vector3f scale = new Vector3f(1, 1, 1);

    protected Sprite sprite;

    protected float spriteOffsetX;
    protected float spriteOffsetY;

    protected int textureIndex = 0;

    public void setPosition(float x, float y, float z){
        position.set(new Vector3f(x, y, z));
    }

    public Vector3f getPosition(){
        return position;
    }

    protected void translate(float xa, float ya){
        position.x += xa;
        position.y += ya;
    }

    public void update(){}
    public void render(){}

    public Vector3f getRotation(){
        return rotation;
    }

    public Sprite getSprite(){
        return sprite;
    }

    public float getSpriteOffsetX(){
        return spriteOffsetX;
    }

    public float getSpriteOffsetY(){
        return spriteOffsetY;
    }

    public void setSpriteOffset(float xoff, float yoff){
        int column = (int) xoff % sprite.getNumberOfRows();
        spriteOffsetX = (float) column / sprite.getNumberOfRows();
        int row = (int) yoff % sprite.getNumberOfRows();
        spriteOffsetY = (float) row / sprite.getNumberOfRows();
    }

    public float getX(){
        return position.x;
    }

    public float getY(){
        return position.y;
    }

    public int getTextureIndex(){ return textureIndex; }

    public float getTextureXOffset(){
        int column = textureIndex % sprite.getNumberOfRows();
        return (float)column/(float) sprite.getNumberOfRows();
    }

    public float getTextureYOffset(){
        int row = textureIndex / sprite.getNumberOfRows();
        return (float) row/(float)sprite.getNumberOfRows();
    }

}
