package org.lwengine.gameobject;

import org.joml.Vector3f;

public class Camera {

    public Vector3f position = new Vector3f(0, 0, 0);

    public Camera(){

    }

    public void setPosition(float xa, float ya){
        position.x = xa;
        position.y = ya;
    }

    public Vector3f getPosition(){
        return position;
    }
}
